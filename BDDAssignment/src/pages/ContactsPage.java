package pages;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utility.Driver;

public class ContactsPage{
	
	public static ContactsPage contacts=null; 

	@FindBy(name="first_name")
	WebElement firstName;
	
	@FindBy(name="last_name")
	WebElement lastName;
	
	@FindBy(name="value")
	WebElement email;
	
	@FindBy(xpath="//button[contains(text(),'New')]")
	WebElement newContact;
	
	@FindBy(xpath="//div[@class='ui warning message']")
	List<WebElement> infoMessage;
	
	@FindBy(xpath="//tbody/tr")
	List<WebElement> tableRows;
	
	@FindBy(xpath="//tbody/tr[1]/td[8]/div/button")
	WebElement deleteicon;
	
	@FindBy(xpath="//button[@class='ui button']")
	WebElement confirmDelete;
	
	@FindBy(xpath="//i[@class='save icon']")
	WebElement saveContact;
	
	@FindBy(xpath="//div[@class='ui warning message']")
	WebElement message;
	
	@FindBy(xpath="//tbody/tr[1]/td[2]")
	WebElement nameText;
	
	@FindBy(xpath="//tbody/tr[1]/td[7]")
	WebElement emailText;
	
	private ContactsPage() {
		PageFactory.initElements(new Driver().getDriver(), this);
	}
	
	public static ContactsPage getContactPage() {
		if(contacts==null) {
			contacts=new ContactsPage();
		}
		return contacts;
	}
	
	public void enterFirstName(String firstname) {
		firstName.sendKeys(firstname);
	}
	
	public void enterLastName(String lastname) {
		lastName.sendKeys(lastname);
	}
	
	public int getElemntCount() {
		return infoMessage.size();
	}
	
	public List<WebElement> getRowElements() {
		return tableRows;
	}
	
	public WebElement verifyMessage() {
		return message;
	}
	
	public void enterEmail(String emailId) {
		email.sendKeys(emailId);
	}
	
	public void clickNewContact() {
		newContact.click();
	}
	
	public void saveContact() {
		saveContact.click();
	}
	
	public void clickDeleteIcon() {
		deleteicon.click();
	}
	
	public void clickConfirmDelete() {
		confirmDelete.click();
	}
	
	public String getName() {
		return nameText.getText();
	}
	
	public String getEmail() {
		return emailText.getText();
	}
}
