package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utility.Driver;

public class LoginPage{
	
	public static LoginPage login=null; 

	@FindBy(name="email")
	WebElement userName;
	
	@FindBy(xpath="//input[contains(@name,'pass')]")
	WebElement password;
	
	@FindBy(xpath="//div[@class='ui fluid large blue submit button']")
	WebElement logIn;
	
	@FindBy(xpath="//div[@class='ui negative message']")
	WebElement errorMsg;
	
	private LoginPage() {
		PageFactory.initElements(new Driver().getDriver(), this);
	}
	
	public static LoginPage getLoginPage() {
		if(login==null) {
			login=new LoginPage();
		}
		return login;
	}
	
	public void enterUserName(String username) {
		userName.sendKeys(username);
	}
	
	public boolean verifyUserNameField() {
		return userName.isDisplayed();
	}
	
	public void enterPassword(String passwrd) {
		password.sendKeys(passwrd);
	}
	
	public HomePage clickLogin() {
		logIn.click();
		return new HomePage();
	}
	
	public void VerifyLoginError() {
		errorMsg.isDisplayed();
	}
}
