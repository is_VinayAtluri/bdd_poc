package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import utility.Driver;

public class HomePage{

	public static HomePage home=null; 
	
	@FindBy(xpath="//span[contains(text(),'Vinay')]")
	WebElement userProfile;
	
	@FindBy(xpath="//div[@role='listbox']")
	WebElement roleSettings;
	
	@FindBy(xpath="//span[contains(text(),'Log Out')]")
	WebElement logOut;
	
	@FindBy(xpath="//span[contains(text(),'Contacts')]")
	WebElement contactsLink;
	
	@FindBy(xpath="//span[contains(text(),'Log In')]")
	WebElement loginBtn;
	
	public HomePage() {
		PageFactory.initElements(new Driver().getDriver(), this);
	}
	
	public static HomePage getHomePage() {
		if(home==null) {
			home=new HomePage();
		}
		return home;
	}
	
	public boolean verifyUser() {
		try {
			return userProfile.isDisplayed();
		}
		catch(Exception e) {
			return false;
		}
		
	}
	
	public void logout() {
		roleSettings.click();
		logOut.click();
	}
	
	public void clickContacts() {
		contactsLink.click();
	}
	
	public void clickLogin() {
		loginBtn.click();
	}
}
