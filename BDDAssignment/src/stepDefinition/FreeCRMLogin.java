package stepDefinition;


import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import pages.HomePage;
import pages.LoginPage;

public class FreeCRMLogin{
	
	
	@When("^user enter \"(.*)\" and \"(.*)\"$")
	public void user_enter_username_and_password(String email, String password) {
		LoginPage.getLoginPage().enterUserName(email);
		LoginPage.getLoginPage().enterPassword(password);
	}

	@When("^user clicks on LogIn button$")
	public void user_clicks_on_LogIn_button() {
		LoginPage.getLoginPage().clickLogin();
	}

	@Then("^Verify user is logged in to account$")
	public void verify_user_is_logged_in_to_account() {
		HomePage.getHomePage().verifyUser();
	}
	
	@Then("^Verify error message displayed$")
	public void verify_login_error_message() {
		LoginPage.getLoginPage().VerifyLoginError();
	}
	
	/*@Then("^logout and close the application$")
	public void logout_close_the_application() {
		if(HomePage.getHomePage().verifyUser()) {
			HomePage.getHomePage().logout();
		}
		Driver.driver.quit();
	}*/
}
