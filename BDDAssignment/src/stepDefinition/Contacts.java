package stepDefinition;

import java.util.List;
import java.util.Map;

import org.openqa.selenium.WebElement;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import junit.framework.Assert;
import pages.ContactsPage;
import pages.HomePage;
import pages.LoginPage;
import utility.Driver;

@SuppressWarnings("deprecation")
public class Contacts{
	
	List<WebElement> rows;
	List<Map<Object, Object>> map=null;
	
	/*@Before("@DeleteContacts")
	public void deleteContacts() throws InterruptedException {
		deletePreviousContacts();
	}*/
	
	@When("user enter userName and Password")
	public void user_enter_userName_and_Password(DataTable dataTable) {
		List<List<String>> list=dataTable.asLists();
		LoginPage.getLoginPage().enterUserName(list.get(0).get(0));
		LoginPage.getLoginPage().enterPassword(list.get(0).get(1));
	}

	@Then("user clicks on contacts link")
	public void user_clicks_on_contacts_link() {
		HomePage.getHomePage().clickContacts();
	}

	@Then("user create new contact using firstName, lastName and email address and save")
	public void user_enters_firstName_lastName_and_email_address_and_click_save(DataTable dataTable) throws InterruptedException {
		map=dataTable.asMaps(String.class, String.class);
		for(Map<Object, Object> data:map) {	
			ContactsPage.getContactPage().clickNewContact();
			Thread.sleep(10000);
			ContactsPage.getContactPage().enterFirstName(data.get("FirstName").toString());
			ContactsPage.getContactPage().enterLastName(data.get("LastName").toString());
			ContactsPage.getContactPage().enterEmail(data.get("Email").toString());
			ContactsPage.getContactPage().saveContact();
			Thread.sleep(10000);
			HomePage.getHomePage().clickContacts();
		}	
	}

	@Then("^Verify \"(.*)\" users added successfully$")
	public void verify_users_added_successfully(String userCount) throws InterruptedException {
		new Driver().getDriver().navigate().refresh();
		Thread.sleep(5000);
		rows=ContactsPage.getContactPage().getRowElements();
		Assert.assertEquals(userCount, String.valueOf(rows.size()));
	}
	
	@Then("delete the contacts created")
	public void delete_the_contacts_created() throws InterruptedException {
		
		Thread.sleep(5000);
		if(ContactsPage.getContactPage().getElemntCount()==0) {
			rows=ContactsPage.getContactPage().getRowElements();
			for(int count=1;count<=rows.size();count++) {
				ContactsPage.getContactPage().clickDeleteIcon();
				ContactsPage.getContactPage().clickConfirmDelete();
				Thread.sleep(5000);
			}
		}else {
			System.out.println("No contacts available");
		}
	}

	@Then("Verify all contacts deleted")
	public void verify_all_contacts_deleted() throws InterruptedException {
		HomePage.getHomePage().clickContacts();
		Thread.sleep(5000);
		Assert.assertTrue(ContactsPage.getContactPage().verifyMessage().isDisplayed());
	}
	
	@Then("verify details are displayed correctly")
	public void fetch_contact_details_and_click_on_view_contact() {	
		Assert.assertEquals(map.get(0).get("FirstName")+" "+map.get(0).get("LastName"),
				ContactsPage.getContactPage().getName());
		Assert.assertEquals(map.get(0).get("Email"),
				ContactsPage.getContactPage().getEmail());
	}
	
	/*public void deletePreviousContacts() throws InterruptedException {
	FreeCRMLogin login=new FreeCRMLogin();
		launchBrowser();
		login.user_is_on_freeCRM_page();
		login.user_enter_username_and_password(prop.getProperty("username"),prop.getProperty("password"));
		login.user_clicks_on_LogIn_button();
		user_clicks_on_contacts_link();
		delete_the_contacts_created();		
		login.logout_close_the_application();
		resetObject();
	}*/
}
