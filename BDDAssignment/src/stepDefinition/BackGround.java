package stepDefinition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import pages.HomePage;
import pages.LoginPage;
import utility.Driver;

public class BackGround{
	Driver driver=new Driver();
	
	@Given("user is on freeCRM login page")
	public void user_is_on_freeCRM_page() {
		driver.getDriver().navigate().to(Driver.prop.getProperty("url"));
		HomePage.getHomePage().clickLogin();
	}
	
	@Then("Verify user is on login page")
	public void verify_user_is_on_loginPage() {
		LoginPage.getLoginPage().verifyUserNameField();
	}
}
