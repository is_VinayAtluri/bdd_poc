package utility;

import java.util.concurrent.TimeUnit;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import pages.ContactsPage;
import pages.HomePage;
import pages.LoginPage;

public class Driver {
	public static WebDriver driver;
	public static Properties prop;
	
	@Before()
	public void openBrowser() {
		System.out.println("Before hook getting executed");
		
		prop=new Properties();
		
		try {
			FileInputStream fis=new FileInputStream("C:\\Users\\vinay.local\\eclipse-workspace\\BDDAssignment\\Resourses\\config.properties");
			prop.load(fis);
		}
		catch(IOException e) {
			System.out.println("Exception while reading properties file");
		}		
		driver=launchBrowser();
	}
	
	@After()
	public void closeBrowser() {
		
		if(HomePage.getHomePage().verifyUser()) {
			HomePage.getHomePage().logout();
		}
		resetObject();
		driver.quit();
	}
	
	public WebDriver getDriver() {
		return driver;
	}

	public WebDriver launchBrowser() {
		
		switch(prop.getProperty("browser")) {
		
		case "Chrome":
			System.setProperty("webdriver.chrome.driver",System.getProperty("user.dir")+"\\drivers\\chromedriver.exe");
			driver=new ChromeDriver();
		}
		
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(Integer.valueOf(prop.getProperty("ImplicitTimeOut")), TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(Integer.valueOf(prop.getProperty("pageLoadTimeOut")), TimeUnit.SECONDS);
		return driver;
	}
	
	public static void resetObject() {
		HomePage.home=null;
		ContactsPage.contacts=null;
		LoginPage.login=null;
	}
}
