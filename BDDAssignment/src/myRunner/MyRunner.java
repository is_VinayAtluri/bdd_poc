package myRunner;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)
@CucumberOptions(
		features="Features",
		glue="stepDefinition",
		plugin= {"pretty","html:test-output","json:test-output/sample.json"},
		dryRun=false,
		monochrome=true,
		tags= {"@login"}
		)
public class MyRunner {

}


