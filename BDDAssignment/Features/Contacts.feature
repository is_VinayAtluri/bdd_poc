Feature: Contact creation functionality

Background: Pre-Requisite for  each scenario
	Given user is on freeCRM login page
	Then Verify user is on login page
   
 
  Scenario: Verify contact details displayed correctly
  
    When user enter userName and Password
      | vinay.jan31@gmail.com | Test@123 |
    And user clicks on LogIn button
    Then Verify user is logged in to account
    Then user clicks on contacts link
    Then user create new contact using firstName, lastName and email address and save
      | FirstName | LastName | Email            |
      | First     | User     | First@gmail.com  |
    Then verify details are displayed correctly
    
    
    
    Scenario: Verify contacts deleted successfully

    When user enter userName and Password
      | vinay.jan31@gmail.com | Test@123 |
    And user clicks on LogIn button
    Then Verify user is logged in to account
    Then user clicks on contacts link
    And delete the contacts created
    Then Verify all contacts deleted
    
    
