Feature: Free CRM login feature

Background: Pre-Requisite for  each scenario
	Given user is on freeCRM login page
	Then Verify user is on login page
	
@login
  Scenario: Verify login success to free CRM application.
    When user enter "vinay.jan31@gmail.com" and "Test@123"
    And user clicks on LogIn button
    Then Verify user is logged in to account
    
  Scenario Outline: Verify invalid login
    When user enter "<email>" and "<password>"
    And user clicks on LogIn button
    Then Verify error message displayed
    
   Examples:
   |email|password|
   |vinay.jan31@gmail.com|Test123|
   |vinay.jan24@gmail.com|Test@123|
